# www2email

www2email server

## History

From 2009 - 2020, I had a [www2email server](http://www.janwagemakers.be/www2email/)
running. I don't run this www2email server anymore, but this is a copy of it.
Note that my www2email server is just an ugly hacked together bash script ;) 

## How?

On a Debian PC, I had a user `www` with the home directory `/home/www`. 
In this home directory there is the file `.forward` with the following
content:

```
| /home/www/mail2queue 
```

So, every mail that user `www` has received is forwarded to the script
`mail2queue`. `mail2queue` puts every mail in the directory `/home/www/queue`.

A cronjob runs on regulary times the script `queue2mail` which reads the
files in `/home/www/queue`, analyse there contents and send out an email with
the content of a webpage as requested.

## Commands

```
www@janwagemakers.be : www --> E-mail

1. what ?
---------

When you send an E-mail to www@janwagemakers.be with a command on the
SUBJECT line, www@janwagemakers.be will send an E-mail back with the content
of a webpage.

Note that www@janwagemakers.be will ignore commands that are not correct.

2. available commands :
-----------------------

2.1 help :
----------

FROM    : you@you.mail.com
TO      : www@janwagemakers.be
SUBJECT : help

- sends this help file to you.
- note : "send help" also works

2.2 send txt http:// :
----------------------

FROM    : you@you.mail.com
TO      : www@janwagemakers.be
SUBJECT : send txt http://en.wikipedia.org/wiki/Silvius_Brabo

- sends a txt with the content of 
  "http://en.wikipedia.org/wiki/Silvius_Brabo" to you.
- note : "send txt en.wikipedia.org/wiki/Silvius_Brabo" will not work.  You
  need to include http:// in the URL.
- note : use "send txt https://" for http-secure pages
- note : use "send txt ftp://" for ftp pages
- note : "send text" also work.

2.3 send txt.zip http:// :
--------------------------

Same as "send txt" but sends the txt in a zip archive.

2.4 send txt.tgz http:// :
--------------------------

Same as "send txt" but sends the txt in a tgz (tar.gz) archive.

2.5 send txt.tbz2 http:// :
---------------------------

Same as "send txt" but sends the txt in a tbz2 (tar.bz2) archive.

2.6 send txt.bz2 http:// :
---------------------------

Same as "send txt" but sends the txt compressed with bzip2.

2.6 send txt.gz http:// :
---------------------------

Same as "send txt" but sends the txt compressed with gzip.

2.7 send pdf http:// :
----------------------

FROM    : you@you.mail.com
TO      : www@janwagemakers.be
SUBJECT : send pdf http://en.wikipedia.org/wiki/Silvius_Brabo

- sends a pdf with the content of
  "http://en.wikipedia.org/wiki/Silvius_Brabo" to you.
- note : "send pdf en.wikipedia.org/wiki/Silvius_Brabo" will not work.  You
  need to include http:// in the URL.
- note : use "send pdf https://" for http-secure pages
- note : use "send pdf ftp://" for ftp pages

2.8 send file http:// :
-----------------------

FROM    : you@you.mail.com
TO      : www@janwagemakers.be
SUBJECT : send file http://www.janwagemakers.be/pics/3com18f452.pdf

- sends the file "http://www.janwagemakers.be/pics/3com18f452.pdf"
- note : "send file www.janwagemakers.be/pics/3com18f452.pdf will not work.
  You need to include http:// in the URL.
- note : use "send zip https://" for http-secure pages
- note : use "send zip ftp://" for ftp pages

2.9 send zip http:// :
----------------------

FROM    : you@you.mail.com
TO      : www@janwagemakers.be
SUBJECT : send zip http://www.janwagemakers.be/wordpress

- sends a zip archive to you with the content from
  "http://www.janwagemakers.be/wordpress". After unpacking you
  can use a normal webbrowser to view the webpage.
- note : "send zip www.janwagemakers.be/wordpress" will not work.
  You need to include http:// in the URL.
- note : there is a download quota of 10.0M. Don't try to fetch webpages with
  a lot of content. 
- you can also use this to get (small) binaries. For example 
  "send zip http://www.janwagemakers.be/tmp/la'chouffe.jpg"
- note : use "send zip https://" for http-secure pages
- note : use "send zip ftp://" for ftp pages

2.10 send tgz http:// :
-----------------------

Same as "send zip", but will send a tgz (tar.gz) archive.

2.11 send tbz2 http:// :
------------------------

Same as "send zip", but will send a tbz2 (tar.bz2) archive.

2.12 send txtsearch word1+word2 :
---------------------------------

Same as send txt http://www.google.com/search?num=100&q=word1+word2

2.13 send txtsearch.gz word1+word2 :
------------------------------------

Same as send txt.gz http://www.google.com/search?num=100&q=word1+word2

2.14 send txtsearch.bz2 word1+word2 :
-------------------------------------

Same as send txt.bz2 http://www.google.com/search?num=100&q=word1+word2

2.15 send txtsearch.zip word1+word2 :
-------------------------------------

Same as send txt.zip http://www.google.com/search?num=100&q=word1+word2

2.16 send txtsearch.tgz word1+word2 :
-------------------------------------

Same as send txt.tgz http://www.google.com/search?num=100&q=word1+word2

2.17 send txtsearch.tbz2 word1+word2 :
--------------------------------------

Same as send txt.tbz2 http://www.google.com/search?num=100&q=word1+word2

3. BUGS :
---------

Yes ;-)

At the moment, this service is just an ugly bash script. There are a lot of
things to improve...

	Send bugs-reports to jan.wagemakers AT gmail.com.

4. www2email mailing list :
---------------------------

You can ask questions about this or other www2email services at the
www2email mailing list <http://groups.google.com/group/www2email>.

- To subscribe: send an email to 'www2email+subscribe@googlegroups.com'
- To post: send an email to 'www2email@googlegroups.com' 
```

## www4mail

On my www2email server I had also a secondary `www4mail server` running.
This software is not written by me, but I'll include the tar.gz files here
in case someone wants to study them.

